# Fibonacci

Fibonacci Example Using ReactJS

# Requirements
- brew installed for MacOS
- yarn/npm install globally

**Note(MacOS):** If brew is not install simply run :

`/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`

More info here: https://brew.sh/ 

# Instructions
- MacOS
    * Homebrew: `brew install yarn`
    * MacPorts: `sudo port install yarn`

**Note:** If running a different OS please follow the installation instructions here :
https://yarnpkg.com/lang/en/docs/install/#mac-stable


# Installation
- Once **yarn** is install, in a console/terminal window navigate to the root folder(fibonacci) 
of the app and run : `yarn install`
- To see the fibonacci implementation in a browser simply run: `yarn start`

# Running Tests
- In a new terminal tab navigate to the root folder of the project and run : 

                yarn test --verbose
                
- To see a coverage report open the index.html located at :

  **/coverage/Icov-report/index.html**