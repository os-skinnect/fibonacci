export enum FibonacciAlgorithm {
    Iteration = "Iteration",
    Recursion = "Recursion"
}