// setup file
import { configure } from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
const jsdom = require( 'jsdom' );
const {performance} = require('perf_hooks');
// @ts-ignore
// Add performance to the global window object
window.performance = performance;
configure({ adapter: new Adapter() });
jest.useFakeTimers();

const copyProps = (src: any, target: any) => {

    const props = Object.getOwnPropertyNames(src)
        .filter(prop => typeof target[prop] === 'undefined')
        .reduce((result, prop) => ({
            ...result,
            [prop]: Object.getOwnPropertyDescriptor(src, prop),
        }), {});
    Object.defineProperties(target, props);
};

jsdom.env('<!doctype html><html><body></body></html>', (err: any, window: any) => {
    global['window'] = window;
    global['document'] = window.document;
    global['navigator'] = {
        userAgent: 'node.js',
    };
    copyProps(window, global);
});