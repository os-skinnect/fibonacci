import * as React from 'react';
import './Fibonacci.css';
import {FibonacciAlgorithm} from "./FibonacciAlgorithm";

/**
 * Defines the property types expected by the Fibonacci component
 */
interface IFibonacciProps  {
    userInput: number;
    startPoint: number;
    algorithm: string,
    resultsHandler?: (results: any) => void
}

/**
 * Defines the state types used within the Fibonacci component
 */
interface IIFibonacciState {
    output: number;
    sequence: number[];
    startPoint: number;
    algorithm: string;
    computeTime: any,
    results: {
        Iteration: [],
        Recursion: []
    }
}

/**
 *
 */
class Fibonacci extends React.Component<IFibonacciProps> {

    /**
     * Let's initialize the component's state
     */
    public state: IIFibonacciState;
    /**
     * If using recursion we use this property to generate the sequence numbers
     */
    private sequence: number[] = [0, 1];

    /**
     * Overrides constructor so we initialize the component's state
     * @param props
     */
    constructor(props: IFibonacciProps) {
        super(props);
        this.state = {
            algorithm: props.algorithm,
            computeTime: 0.0,
            output: 0,
            results: {
                Iteration: [],
                Recursion: []
            },
            sequence: [],
            startPoint: props.startPoint
        };
    }

    /**
     * Checks weather we should generate the sequence using either Iteration or Recursion and updates the component's
     * state to render the sequence and output
     */
    public componentWillMount() {

        this.componentWillReceiveProps(this.props);
    }

    /**
     * When the parent component updates the form we want to run our Fibonacci algorithm again based on the new props
     * passed
     * @param nextProps
     */
    public componentWillReceiveProps(nextProps: IFibonacciProps) {

        let results: any;

        if ( nextProps.algorithm === FibonacciAlgorithm.Iteration ) {
            const start = performance.now();
            const arr = this.doIteration(nextProps.userInput, nextProps.startPoint);
            const end = performance.now();
            const out = arr[nextProps.userInput];
            const computeTime = (end - start).toFixed(4);
            // Finally let's update the state so we render the sequence and display the output
            this.setState({
                algorithm: nextProps.algorithm,
                computeTime: computeTime + ' ms',
                output: out,
                sequence: arr,
                startPoint: nextProps.startPoint
            });
            results = {
                fn: nextProps.userInput,
                output: out,
                time: computeTime + ' ms'
            };

        } else {
            // Recursion it is
            this.sequence = [0, 1];
            const start = performance.now();
            const out = this.doRecursion(nextProps.userInput);
            const end = performance.now();
            const computeTime = (end - start).toFixed(4);
            // Finally let's update the state so we render the sequence and display the output
            this.setState({
                algorithm: nextProps.algorithm,
                computeTime: computeTime + ' ms',
                output: out,
                sequence: this.sequence,
                startPoint: nextProps.startPoint
            });
            results = {
                fn: nextProps.userInput,
                output: out,
                time: computeTime + ' ms'
            };
        }

        const stateResults = this.state.results;
        stateResults[nextProps.algorithm].push(results);
        this.setState({results: stateResults});
    }

    public render() {

        return (
            <div className="Fibonacci-content">

                <div className="App-iteration-results">
                    <strong>Iteration Results</strong>
                    {this.state.results.Iteration.map( (result: any, index: number) => (
                        <p key={index}>
                            {index + 1}.- F(n) = {result.fn} | out: {result.output} | time = {result.time} </p>
                    ))}
                </div>

                <div className="App-recursive-results">
                    <strong> Recursion Results </strong>
                    {this.state.results.Recursion.map( (result: any, index: number) => (
                        <p key={index}>
                            {index + 1}.- F(n) = {result.fn} | out: {result.output} | time = {result.time}
                        </p>
                    ))}
                </div>

                User Input: <span className="App-highlighted-text">{this.props.userInput}</span> |
                Start Point: <span className="App-highlighted-text">{this.state.startPoint}</span>
                <div className="Fibonacci-sequence">
                    {this.validateStartingPoint()
                        ? this.state.sequence.map( (next: number, index: number) => (
                            <div key={index} className="Fibonacci-number">{next}</div>
                        ))
                        : this.renderInvalidStartingPoint()
                    }
                </div>
                <div className="Fibonacci-output">
                    <div>Algorithm Used: {this.state.algorithm}
                        <span style={{fontSize: 'small', marginLeft: '5px'}}>
                            : {this.state.computeTime}
                        </span>
                    </div>
                    <div className="App-highlighted-text">
                        <strong>OUTPUT : </strong> {this.state.output}
                    </div>
                </div>
            </div>
        );
    }

    /**
     * Displays an error message if the starting point is invalid
     */
    public renderInvalidStartingPoint() {
        return (
            <div className="Fibonacci-invalid">
                The starting point provided is invalid. By definition, the first two numbers in the Fibonacci sequence
                are either 1 and 1, or 0 and 1
            </div>
        );
    }

    /**
     * Validates if the starting point given is valid
     */
    public validateStartingPoint(): boolean {
        // By definition, the first two numbers in the Fibonacci sequence are either 1 and 1, or 0 and 1
        return ( this.props.startPoint >= 0 && this.props.startPoint <= 1);
    }

    /**
     * Gets the fibonacci sequence numbers using the starting point and the userInput from the props
     */
    public doIteration(userInput: number, startPoint: number): number[] {

        if ( this.validateStartingPoint() ) {
            /**
             * Fibonacci numbers: numbers characterized by the fact that every number after the first two is the sum
             * of the two preceding ones.
             */
            // Let's initialize the sequence with the starting point from the props (either 0 or 1)
            const sequence: number[] = [startPoint, 1];
            for (let i = 2; i < userInput + 1; i++) {
                // after the first two the following number should be the sum of the two preceding ones
                const next = sequence[i - 1] + sequence[i - 2];
                sequence.push(next);
            }

            return sequence;
        }

        return [];
    }

    /**
     *
     * @param n
     * @param memoization
     */
    public doRecursion(n: number, memoization?: any): number {

        if ( !this.validateStartingPoint() ) {
            return 0;
        }

        // If we don't receive a memoization function let's initialize it
        memoization = memoization || {};

        // Have we computed F(n) ?
        if ( memoization[n] ) {
            return memoization[n];
        }

        // Safe guard so we don't end up in an endless recursion
        if ( n <= 1 ) {
            return n;
        }

        // If we reach this point we haven't calculated F(n). Let's do our recursion and save it in memory for later
        memoization[n] =  this.doRecursion( n -1, memoization) + this.doRecursion(n - 2, memoization);
        // Let's also save this in the component's variable so we can update our state once we're done here and show the
        // Fibonacci sequence
        this.sequence.push(memoization[n]);

        return memoization[n];
    }
}

export default Fibonacci;
