import * as React from 'react';
import {mount, ReactWrapper} from 'enzyme';
import App from '../App';
import {FibonacciAlgorithm} from "../FibonacciAlgorithm";
import Fibonacci from "../Fibonacci";
import {Component} from "react";

let wrapper: ReactWrapper;
let instance: Component;

describe('Tests App Component', () => {

    beforeAll( () => {
        // I usually would do this using shallow since I want to test my component in isolation but enzyme does not have
        // support for refs. The app component is using refs to get the value of each input
        wrapper = mount(<App/>);
        instance = wrapper.instance();
    });

    it('Tests Initial State', () => {
        // @ts-ignore
        expect(instance.state.algorithm).toBe(FibonacciAlgorithm.Iteration);
        // @ts-ignore
        expect(instance.state.userInput).toBe(10);
        // @ts-ignore
        expect(instance.state.shouldSimulate).toBe(false);
    });

    it ('Renders a Fibonacci Component', () => {

        const fibonacci = wrapper.find(Fibonacci);
        const props = fibonacci.props();

        expect(fibonacci.exists()).toBeTruthy();
        // @ts-ignore
        expect(props.algorithm).toBe(instance.state.algorithm);
        // @ts-ignore
        expect(props.userInput).toBe(instance.state.userInput);
        // @ts-ignore
        expect(props.startPoint).toBe(instance.state.startPoint);
    });

    it( 'Tests computeSequence Is Called After Click', () => {

        // @ts-ignore
        const spy = jest.spyOn(instance, 'computeSequence');
        // @ts-ignore
        const simulate = jest.spyOn(instance, 'simulate');
        instance.forceUpdate();
        const button = wrapper.find('button');
        button.simulate('click');
        expect(spy).toHaveBeenCalled();
        // simulate function should not be called after click(shouldSimulate initial state is false)
        expect(simulate).not.toHaveBeenCalled();
    });
});
