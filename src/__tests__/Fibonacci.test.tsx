import * as React from 'react';
import {shallow, ShallowWrapper} from 'enzyme';
import {FibonacciAlgorithm} from "../FibonacciAlgorithm";
import Fibonacci from "../Fibonacci";
import {Component} from "react";

let wrapper: ShallowWrapper;
let instance: Component;

describe('Tests Fibonacci Component', () => {

    it('Tests Fibonacci Sequence Using Iteration', () => {
        // Shallow component
        wrapper = shallow(
            <Fibonacci
                algorithm={FibonacciAlgorithm.Iteration}
                userInput={10}
                startPoint={0}
            />
        );
        instance = wrapper.instance();

        // @ts-ignore
        expect(instance.props.algorithm).toBe(FibonacciAlgorithm.Iteration);
        // @ts-ignore
        expect(instance.props.userInput).toBe(10);
        // @ts-ignore
        expect(instance.props.startPoint).toBe(0);

        // Based on the userInput(10) the Fibonacci sequence should have 11 numbers
        const fibonacciNumber = wrapper.find('.Fibonacci-number');
        expect(fibonacciNumber).toHaveLength(11);
        // Expected sequence
        const sequence = ['0', '1', '1', '2', '3', '5', '8', '13', '21', '34', '55'];
        fibonacciNumber.map( (num: ShallowWrapper, index: number) => {
            expect(num.text()).toBe(sequence[index]);
        });
        // F(10) = 55
        expect(fibonacciNumber.last().text()).toBe('55');
        // @ts-ignore
        expect(instance.state.output.toString()).toBe('55');
    });

    it('Tests Fibonacci Sequence Using Recursion', () => {
        // Shallow component
        wrapper = shallow(
            <Fibonacci
                algorithm={FibonacciAlgorithm.Recursion}
                userInput={20}
                startPoint={0}
            />
        );
        instance = wrapper.instance();
        // @ts-ignore
        expect(instance.props.algorithm).toBe(FibonacciAlgorithm.Recursion);
        // @ts-ignore
        expect(instance.props.userInput).toBe(20);
        // @ts-ignore
        expect(instance.props.startPoint).toBe(0);

        // Based on the userInput(20) the Fibonacci sequence should have 21 numbers
        const fibonacciNumber = wrapper.find('.Fibonacci-number');
        expect(fibonacciNumber).toHaveLength(21);

        // Expected sequence
        const sequence = [
            '0', '1', '1', '2', '3', '5', '8', '13', '21', '34', '55', '89', '144', '233', '377', '610',
            '987', '1597', '2584', '4181', '6765'
        ];
        fibonacciNumber.map( (num: ShallowWrapper, index: number) => {
            expect(num.text()).toBe(sequence[index]);
        });
        // F(20) = 6765
        expect(fibonacciNumber.last().text()).toBe('6765');
        // @ts-ignore
        expect(instance.state.output.toString()).toBe('6765');

    });

    it('Tests Invalid Message Is Shown', () => {
        // Shallow component
        wrapper = shallow(
            <Fibonacci
                algorithm={FibonacciAlgorithm.Recursion}
                userInput={20}
                startPoint={10}
            />
        );
        // Fibonacci numbers should start with a 0 or a 1. Passing a different startPoint should show an invalid message
        const invalid = wrapper.find('.Fibonacci-invalid');
        expect(invalid.exists()).toBeTruthy();
    });
});