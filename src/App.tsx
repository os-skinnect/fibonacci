import * as React from 'react';
import './App.css';
import Fibonacci from "./Fibonacci";
import logo from './logo.svg';
import {FibonacciAlgorithm} from "./FibonacciAlgorithm";

interface IAppState {
    algorithm: string;
    shouldSimulate: boolean;
    startPoint: number;
    userInput: number;
}

class App extends React.Component {

    /**
     * Let's set the component's initial state
     */
    public state: IAppState = {
        algorithm: FibonacciAlgorithm.Iteration,
        shouldSimulate: false, // Change this to true if you want the program to automatically compute
        startPoint: 0,
        userInput: 10,
    };
    private readonly userInputRef: any;
    private readonly algorithmRef: any;
    private runs: number = 0;
    private simulator: any;

    constructor(props: any) {
        super(props);
        // Create Our References
        this.userInputRef = React.createRef();
        this.algorithmRef = React.createRef();
        // Do our bindings
        this.computeSequence = this.computeSequence.bind(this);

    }

    /**
     * Once the component has mounted we can have access to our references to update their values with the ones in the
     * initial state
     */
    public componentDidMount() {
        this.algorithmRef.current.value = this.state.algorithm;
        this.userInputRef.current.value = this.state.userInput;
    }

    /**
     * Release resources to avoid memory leaks
     */
    public componentWillUnmount() {
        clearInterval(this.simulator);
    }

    /**
     * Updates the state with the values in the references so the Fibonacci component computes the sequence again
     */
    public computeSequence() {

        if ( this.state.shouldSimulate ) {
            this.runs = 0;
            this.userInputRef.current.value -= 10;
            this.simulate();
        } else {
            this.setState({
                algorithm: this.algorithmRef.current.value,
                userInput: parseInt(this.userInputRef.current.value, 0)
            });
        }
    }

    public render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <h1 className="App-title">Fibonacci Exercise</h1>
                    <div style={{color: '#ccc', marginBottom: '10px'}}>
                        Use the controls below and click on compute to generate the Fibonacci Sequence
                    </div>
                </header>
                {/*User Input*/}
                <div className="App-state-inputs">
                    F(n):
                    <span style={{marginLeft: 5}}>
                        <input
                            ref={this.userInputRef}
                            type="number"
                            placeholder="# of iterations"
                            name='userInput'
                            className="form-control"
                        />
                    </span>
                    <span style={{marginLeft: 5}}>Algorithm:</span>
                    <span style={{marginLeft: 5}}>
                        <select
                            ref={this.algorithmRef}
                            className="form-control"
                            // onChange={this.algorithmOnChange}
                        >
                            <option>{FibonacciAlgorithm.Iteration}</option>
                            <option>{FibonacciAlgorithm.Recursion}</option>
                        </select>
                    </span>
                    <span style={{marginLeft: 5}}>
                        <button onClick={this.computeSequence}>Compute</button>
                    </span>
                </div>
                {/*Fibonacci Component*/}
                <div className="App-content">
                    <Fibonacci
                        algorithm={this.state.algorithm}
                        startPoint={this.state.startPoint}
                        userInput={this.state.userInput}
                    />
                </div>
            </div>
        );
    }

    /**
     * Creates an interval and calculates Fibonacci numbers increasing the userInput by 10 each run
     */
    private simulate() {
        this.simulator = setInterval(() => {

            if ( this.runs < 6 ) {
                this.userInputRef.current.value = parseInt(this.userInputRef.current.value, 0) + 10;
                this.setState({
                    algorithm: this.algorithmRef.current.value,
                    userInput: parseInt(this.userInputRef.current.value, 0)
                });
                this.runs++;
            } else {
                alert(this.state.algorithm + ' Algorithm Finished ');
                clearInterval(this.simulator);
            }

        }, 1000);
    }
}

export default App;
